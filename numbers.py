def convert(num):
    units = [
        "", "մեկ ", "երկու ", "երեք ", "չորս ", "հինգ ", "վեց ", "յոթ ", "ութ ", "ինը ", "տաս ", "տասնմեկ ",
        "տասներկու ",
        "տասներեք ", "տասնչորս ", "տասնհինգ ", "տասնվեց ", "տասնյոթ ", "տասնութ ", "տասնինը "]
    tens = ["", "", "քսան ", "երեսուն ", "քառասուն ", "հիսուն ", "վաթսուն ", "յոթանասուն ", "ութսուն ", "իննսուն "]

    if num < 0:
        return "մինուս " + convert(-num)

    if num < 20:
        return units[num]

    if num < 100:
        return tens[num // 10] + units[int(num % 10)]

    if num < 1000:
        string = ""
        if (num // 100) > 1:
            string += units[num // 100]
        string += "հարյուր " + convert(int(num % 100))
        return string

    if num < 1000000:
        string = ""
        if (num // 1000) > 1:
            string += convert(num // 1000)
        string += "հազար " + convert(int(num % 1000))
        return string

    if num < 1000000000:
        string = ""
        if (num // 1000000) > 1:
            string += convert(num // 1000000)
        string += "միլիոն " + convert(int(num % 1000000))
        return string

    string = ""
    if (num // 1000000000) > 1:
        string += convert(num // 1000000000)
    string += "միլիարդ " + convert(int(num % 1000000000))
    return string


inputNumber = input("Enter your number: ")
if "." in inputNumber:
    floatNumber = round(float(inputNumber), 2)
    splitNumber = str(floatNumber).split(".")
    leftSide = convert(int(splitNumber[0]))
    rightSide = convert(int(splitNumber[1]))
    print(leftSide + " ամբողջ " + rightSide + " հարուրերորդական")
else:
    print(convert(int(inputNumber)))

